1 < 6 && 3 == 3
2 != 6 && 2 != 2

Tabla de la verdad AND:

C1  C2	Res
F	F	F
V	F	F
F	V	F
V	V	V

1 < 6 || 3 == 3
2 != 6 || 2 != 2
3 >= 4 || false

Tabla verdad OR ( || )
C1  C2	Res
F	F	F
V	F	V
F	V	V
V	V	V

!(4 == 4)
!(3 < 1)

3 ==10 || !(5 == 10)
(3 > 3 || 6 == 6) && !(5 ==5)
!(4 < 10) || 10 == 3 && 2 == 2

