-- Seleccionar esta bbdd para no tener que ponerlo tol rato
USE bd_correos;
-- Consulta de tooooodos los datos
SELECT * FROM envio_carta;
-- Consulta limitada: usando LIMIT
SELECT * FROM envio_carta LIMIT 3;
-- Condicionadas: usando WHERE
	-- La más exacta que devuelve sólo un registro:
SELECT * FROM envio_carta WHERE id=5;
	-- Consultas que pueden devolver más de un registro:
    -- Consultas para campos numéricos, usando condiciones matem.
SELECT * FROM envio_carta WHERE CP=3005;
SELECT * FROM envio_carta WHERE CP>=3000 AND CP <= 3999;
	-- Consultas para campos de texto, usan condiciones
SELECT * FROM envio_carta WHERE nombre_destinatario='Fulano';
SELECT * FROM envio_carta WHERE nombre_remitente='Fulano';
SELECT * FROM envio_carta WHERE nombre_remitente>'H';	-- Lo busca alfabeticamente
SELECT * FROM envio_carta WHERE direccion_entrega LIKE '%Calle%';
	-- OR se le como 'o': Selecciona las cartas donde direccion_entrega contenga 'Calle' o bien
    -- el metitente sea Fulanita: En cuanto una de las dos condiciones sea verdadera, es reg. es incluido
SELECT * FROM envio_carta WHERE direccion_entrega LIKE '%Av%' OR nombre_remitente='Fulanita' OR CP=3111;
	-- Con AND sin embargo es obligatorio que las dos condiciones sean verdaderas
SELECT * FROM envio_carta WHERE direccion_entrega LIKE '%Calle%' AND nombre_destinatario='Fulanita' AND nombre_remitente='Fulano';
SELECT * FROM envio_carta WHERE id_usuario_destinatario=2;
SELECT * FROM envio_carta, usuario WHERE envio_carta.id_usuario_destinatario = usuario.id AND usuario.edad > 40;
SELECT * FROM envio_carta, usuario WHERE envio_carta.id_usuario_remitente = usuario.id AND usuario.edad < 40;