-- Destruir y arrancar MySQL:
-- sudo docker rm -f mi_mysql
-- sudo docker run --name mi_mysql  -d -p 3306:3306  -e MYSQL_ROOT_PASSWORD=123qwe  --mount src=mysql-db-data,dst=/var/lib/mysql   mysql:8.0.21
-- sudo docker start  mi_mysql

DROP DATABASE IF EXISTS `bd_correos`;
CREATE DATABASE `bd_correos`;
-- CREATE SCHEMA  `bd_correos`;

DROP TABLE IF EXISTS `bd_correos`.`usuario`;
-- DROP TABLE  `bd_correos`.`usuario`;
CREATE TABLE `bd_correos`.`usuario` (
		-- INT admite -2.000.000.000 al 2.000.000.000
	id INT unsigned NOT NULL auto_increment,
		-- NOT NULL: No sea NULL el campo (que no es lo mismo que cadena vacía)
		-- ¿NULL?: Es el dato especial para inidicar que el campo
		-- 		no sea ha inicializado a ningún valor
    email VARCHAR(320) NOT NULL,
    nombre VARCHAR(50) NOT NULL,
		-- El acento grave en el nombre es opcional cuando el nombre (de tabla, bbdd, campo...) NO TIENE espacios
    `direccion` VARCHAR(100), 	-- TINYINT UNSIGNED van del 0 al 255		
    edad TINYINT UNSIGNED,        
		-- Para evitar registros duplicados necesitamos crear un índice ((una especie
        -- de diccionario con tooooodos los emais organizados EN ORDEN e indicar que 
        -- es de tipo UNIQUE: evitar valores duplicados
    UNIQUE INDEX id_UNIQUE (id ASC) VISIBLE,
		-- Creamos otro índice para indicar que es clave primario
	PRIMARY KEY pk_id (id),
    UNIQUE INDEX email_UNIQUE (email ASC) VISIBLE
);	
	-- Insertamos datos: 	
INSERT INTO `bd_correos`.`usuario` (email, nombre, direccion, edad)		
	VALUES ('Fulano', 'ful@email.es', 'Calle Total 12', 62);				
    -- Los datos Sí VAN con COMILLA SIMPLE
INSERT INTO `bd_correos`.`usuario` (email, nombre, direccion, edad)
	VALUES ('Mengano', 'men@email.es', 'Av Alli mismo 3434', 33);
SELECT * FROM `bd_correos`.`usuario`;
