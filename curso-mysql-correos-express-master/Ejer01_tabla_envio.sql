-- Ejercicio: Crear una tabla `envio_carta`  id como PK autoincrementable no nulo, 
-- `remitente` no nulo, destinatario no nulo, direcccion_entrega no nulo, 
-- CP no nulo del tipo numerico que se ajuste a los CP de España

DROP TABLE IF EXISTS `bd_correos`.`envio_carta`;
CREATE TABLE `bd_correos`.`envio_carta` (
	id INT unsigned NOT NULL auto_increment,
    nombre_remitente VARCHAR(50) NOT NULL,
    nombre_destinatario VARCHAR(50) NOT NULL,
    `direccion_entrega` VARCHAR(100), 
    -- SMALLINT sólo ocupa 2 bytes
    CP SMALLINT UNSIGNED,     
    -- Clave foránea a usuario (referencia a esa tabla)
    id_usuario_remitente INT unsigned,
    id_usuario_destinatario INT unsigned,
    
    UNIQUE INDEX id_UNIQUE (id ASC) VISIBLE,
	PRIMARY KEY pk_id (id),
    -- foreign key tenemos que indicar el campo de esta tabla entre paréntesis,
    -- y luego, la tabla a la que referencia y entre paréntesis el campo al que apunta
    FOREIGN KEY fk_id_usuario_remit(id_usuario_remitente) references usuario(id),
    FOREIGN KEY fk_id_usuario_dest(id_usuario_destinatario) references usuario(id)
);	
	-- Insertamos datos: 	
INSERT INTO `bd_correos`.`envio_carta` (nombre_remitente, nombre_destinatario, direccion_entrega, CP, id_usuario_remitente, id_usuario_destinatario)		
	VALUES ('Fulano', 'Mengano', 'Av Alli mismo 3434', 03005, 1, 2);
    -- Los datos Sí VAN con COMILLA SIMPLE
INSERT INTO `bd_correos`.`envio_carta` (nombre_remitente, nombre_destinatario, direccion_entrega, CP, id_usuario_remitente, id_usuario_destinatario)
	VALUES ('Mengano', 'Fulano', 'Calle Total 12', 07007, 2, 1);
INSERT INTO bd_correos.envio_carta (nombre_remitente, nombre_destinatario, direccion_entrega, CP, id_usuario_remitente)
	VALUES ('Mengano', 'Fulanita', 'Calle Totality 25', 09009, 2);
INSERT INTO bd_correos.envio_carta (nombre_remitente, nombre_destinatario, direccion_entrega, CP, id_usuario_destinatario)
	VALUES ('Fulanita', 'Mengano', 'Av Alli mismo 3434', 03005, 2);
INSERT INTO bd_correos.envio_carta (nombre_remitente, nombre_destinatario, direccion_entrega, CP, id_usuario_remitente)
	VALUES ('Fulano', 'Fulanita', 'Calle Totality 25', 09009, 1);
-- Actualizamos un registros
UPDATE `bd_correos`.`envio_carta` SET `CP` = '3111' WHERE (`id` = '2');

SELECT * FROM `bd_correos`.`envio_carta`;
