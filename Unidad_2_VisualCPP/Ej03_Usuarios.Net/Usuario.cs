﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej03_Usuarios.Net {
    class Usuario {

		int id;
		string nombre;
		string email;
		string direccion;
		int edad;

		public Usuario(int id, string nombre, string email, string direcc, int edad) {
			this.id = id;
			this.nombre = nombre;
			this.email = email;
			this.direccion = direcc;
			this.edad = edad;
		}

		public string ToString() {
			string texto = nombre + ", " + this.email;
			return texto;
		}

		// Getter y Setter: Métodos típicos para cambiar y leer propiedades
		public string GetNombre() {
			return this.nombre;
		}
		public string GetEmail() {
			return this.email;
		}
		public string GetDireccion() {
			return this.direccion;
		}
		public void SetDireccion(string nuevaDirecc) {
			this.direccion = nuevaDirecc;
		}

		// Método tipo inline declarado e implementado en la cabecera *.h ó *.hpp
		/* Como la función es inline, el código binario se copia allá donde se use:
		- Esta función es más rápida, pero ocupa más bytes */
		public bool DatosValidos() {
			string email = this.GetEmail();
			int posArroba = email.IndexOf('@', 1);	// Igual que find
			return this.GetNombre().Length > 1
				&& this.GetEmail().Length >= 3
				&& posArroba >= 1 /*&& posArroba != string::npos*/;
		}
	}
}
