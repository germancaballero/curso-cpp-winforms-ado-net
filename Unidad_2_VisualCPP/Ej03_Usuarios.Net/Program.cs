﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej03_Usuarios.Net {
    class Program {
        /* static es para indicar que una función independiente de la clase, es decir, es una función que NO es de clase, viene a ser lo más parecido a las funciones individuales de C y C++*/
        static void Main(string[] args) {
            Console.WriteLine("Ejemplo C# de usuarios");

            // Aquí en C#, new siempre reserva memoria, y las variables de tipo objeto SIEMPRE son REFERENCIAS
            // Las referencias en el fondo son punteros que no nos preocupar de gestionar
            Usuario comprador = new Usuario(1, "Felipe", "felipe@email.com", "Alli 20", 44);
            Console.WriteLine(comprador.ToString());
            if (comprador.DatosValidos()) {
                Console.WriteLine("Datos comprador " + comprador.GetNombre() + " validos" );
            } else {
                Console.WriteLine("Datos comprador " + comprador.GetNombre() + " NO validos");
            }
            Console.ReadKey();
        }
    }
}
