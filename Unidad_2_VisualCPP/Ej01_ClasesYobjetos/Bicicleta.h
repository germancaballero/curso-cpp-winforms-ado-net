#pragma once
// string proporciona una clase string para simplificar el trabajo con cadenas de caracteres. En el fondo son array char. 
    // string est�n en el namespace "std"
#include <string>

// Las clases deben empezar en mayusculas. El resto (propiedades y m�todos, enCamelCase.
class Bicicleta
{
	// PROPIEDADES (VARIABLES MIEMBRO):

	// Usando el namespace std::<clase, funci�n, constante...>
public:	// nos permite acceder a propiedes y/o m�todos y/o constructores 
	std::string marca;

private:	// Nivel de acceso privado: s�lo se puede usar a nivel interno de la clase. Esto es la ENCPAPSULACI�N
	int marchas;
	float precio;

public:
	// Declaraci�n FUNCIONES CONSTRUCTORAS:  Un constructor es una funci�n especial que se llama la primera vez, cuando se reserva la memoria para albergar el objeto. Como funci�n tiene el mismo nombre que la clase y suele recibir par�metros que ser�n las propiedades
	Bicicleta(std::string marca, int marchas, float precio);
	// El destructor es la funci�n quje se llama cuando el objeto es liberado de memoria 
	~Bicicleta();

	// Declaraci�n M�TODOS: Funciones exclusivas de esta clase
	void mostrar();
};

