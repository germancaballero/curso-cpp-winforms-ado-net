#include "Bicicleta.h"
#include <iostream>

// Aqu� implementamos (definimos) los constructores, no llevan tipo devuelto
// NombreClase::NombreClase(par�metro...) { 
//    ... c�digo ....
// }
Bicicleta::Bicicleta(std::string marca, int marchas, float precio) {
	// Este m�todo constructor se llama una vez que se ha reservado la memoria del objeto. Es donde "this" empieza a apuntar al objeto instanciado. Por lo tanto, lo usaremos para rellenar los datos
	this->marca = marca;
	this->marchas = marchas;
	this->precio = precio;
}

// Aqu� implementamos (definimos) los m�todos (funciones)
// <tipo_devuelto> NombreClase::nombreMetodo(par�metro...) { 
//    ... c�digo ....
// }

void Bicicleta::mostrar() {
	std::cout << "Bici " << this->marca << std::endl;
	std::cout << " - Marchas: " << this->marchas << std::endl;
	std::cout << " - Precio: " << this->precio << std::endl;
}

// Destructor: Aqu� debemos liberar la memoria de las propiedades

Bicicleta::~Bicicleta() {
	std::cout << "Adios a la bici " << this->marca << std::endl;
	// free, deletes...
}