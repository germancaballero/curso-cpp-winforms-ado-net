// stream: Flujo: flujo de datos
// io: input/output
#include <iostream>
#include "Bicicleta.h"

void crear_y_destruir_bici_vecino();
void crear_y_destruir_bici_amiga();

int main()
{
    // iostream están en el namespace "std"
    // Un namespace (espacio de nombres) es un conjunto de funcionalidades agrupadas.
    std::cout << "Unidad " << 1 << ": Clases y objetos\n";

    // Crear una clase, cuya reserva de memoria es gestionada por el compilador
    std::string nombreBici = "BH";
    // Creamos la variable usando el usando el constructor
    Bicicleta miBici(nombreBici, 21, 275);
    // Invocamos con el '.' al método mostrar, añadiendole los paréntesis '()' cómo en las funciones.
    miBici.mostrar();

    crear_y_destruir_bici_vecino();
    crear_y_destruir_bici_amiga();
}


void crear_y_destruir_bici_vecino() {
    // Aquí el compilador se encarga de crear el código binario para reservar la memoria de la bici del vecino: es decir, los 20 ó 30 bytes para la bici, más los 20 bytes aprox. del texto marca.
    Bicicleta biciVecino("Bici vieja y oxidada", 3, 25);
    biciVecino.mostrar();
    // Por aquí, como ocurre con las variables normales (primitivas, int, float, structs...) el compilador también se encarga de liberar la memoria
}

void crear_y_destruir_bici_amiga() {
    // Nosotros nos vamos a encargar de reservar y liberar la memoria para la bici.
    Bicicleta* biciAmiga = NULL;
    // En lugar "malloc" para la reserva usamos "new"
    biciAmiga = new Bicicleta("Bici guay", 24, 500);
    // A la hora de usar los métodos, al igual que los structs, con ->
    biciAmiga->marca = "Super bici chachi";
    biciAmiga->mostrar();
    // siempre siempre SIEMPRE hay que liberar memoria
    delete biciAmiga;   // Que es como free()
}
/* Ejercicios 3:
* 1 - Crear otra bici nueva como variable normal
* 2 - Añadir un campo a bicicleta
* 3 - Añadir un método a bicleta
* 4 - Crear otra bici nueva pero como puntero
* 5 - Una nueva clase de lo que queráis (Coche, Frutas, Peliculas...) y usarla con un par de objetos
*/
