#pragma once
#include <string>

using namespace std;

class Usuario
{
	// Las propiedades son privadas por defecto. �Cuando deber�an ser privadas las propiedades? Siempre
	int id;
	string nombre;
	string email;
	string direccion;
	int edad;

public:
	Usuario(int id, string nombre, string email, string direcc, int edad);
	string toString();
	
	// Getter y Setter: M�todos t�picos para cambiar y leer propiedades
	string getNombre();
	string getEmail();
	string getDireccion();
	void setDireccion(string nuevaDirecc);

	// M�todo tipo inline declarado e implementado en la cabecera *.h � *.hpp
	/* Como la funci�n es inline, el c�digo binario se copia all� donde se use:
	- Esta funci�n es m�s r�pida, pero ocupa m�s bytes */
	bool inline datosValidos() {
		string email = this->getEmail();
		size_t posArroba = email.find('@', 1);
		return this->getNombre().length() > 1
			&& this->getEmail().length() >= 3
			&& posArroba >= 1 && posArroba != string::npos;
	}
};

