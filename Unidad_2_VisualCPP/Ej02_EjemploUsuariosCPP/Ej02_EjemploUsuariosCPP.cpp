#include <iostream>
#include "Usuario.h"

int main()
{
    std::cout << "Usuarios:\n";

    Usuario comprador(1, "Felipe", "felipe@email.com", "Alli 20", 44);
    cout << endl << comprador.toString();

    // Los datos son v�lidos cuando el email tiene m�nimo a@a, El nombre m�s de una letra y la edad sea positiva y menor que 150
    if (comprador.datosValidos()) {
        cout << endl << "Datos comprador " << comprador.getNombre() << " validos" << endl;
    }
    else {
        cout << endl << "Datos comprador " << comprador.getNombre() << " NO validos" << endl;
    }
    /* Ahora creamos un puntero que apunta a la dirrecc mem donde est� el espacio reservado con el tama�o para un usuario, gracias a new */
    Usuario* vendedor = new Usuario(1, "Juan", "@juanemail.com", "Alli 50", 44);
    cout << endl << vendedor->toString();
    // 5 + 2 * 2        (true || true) && false
    // Esta vez usamnos el operador ternario:   <condicion> ? <datos_si_verdad> : <datos_si_false>
    cout << endl << "Datos usuario " << vendedor->getNombre()
        <<  (vendedor->datosValidos() ? "" : " NO ") << " validos" << endl;
    delete vendedor;    // Como el free, libera la memoria del vendedor
}
// Crear un nuevo usuario 
// implementar un m�todo que valide la edad y usarlo a la hora de validar los datos
// Los datos no son v�lidos si la edad no es v�lida3