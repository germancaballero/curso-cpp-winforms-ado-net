#include "Usuario.h"

Usuario::Usuario(int id, string nombre, string email, string direcc, int edad) {
	this->id = id;
	this->nombre = nombre;
	this->email = email;
	this->direccion = direcc;
	this->edad = edad;
}

string Usuario::toString() {
	string texto = nombre + ", " + this->email;
	return texto;
}

string Usuario::getNombre() {
	return this->nombre;
}
string Usuario::getEmail() {
	return this->email;
}
string Usuario::getDireccion() {
	return this->direccion;
}
void Usuario::setDireccion(string nuevaDirecc) {
	this->direccion = nuevaDirecc;
}