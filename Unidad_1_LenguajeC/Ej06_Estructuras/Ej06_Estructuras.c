#include <stdio.h>
#include <string.h>
// Mientras los arrays son tipos de datos en los que una variable puede albergar varios valores del mismo tipo, y se acceden a los valores mediante un índice (entero que indica la posición).
// Las estructuras son tipos de datos pero donde cada valor puede ser de un tipo diferente, y se accede a él a través de un nombre, y como norma general, están relacionados sobre la misma entidad (cosa, objeto, concepto..).
/*struct [nombre structure] {

   definición de miembro (tipo, nombre...);
   definición de miembro;
   ...
   definición de miembro;
} [una o más variables de la estructura; 
Usuario: nombre, email, clave, dirección, edad*/
struct Usuario
{
	char nombre[50];
	char email[321];	// 320 del email, o para el caracter nulo \0 del final
	char direccion[101];// 100 caracteres
	int edad;
};
struct Coche {
	char* marca;
	int marcaTam;
	char* modelo;
	int tamModel;

};
//typedef struct Usuario Usuario;

void mostrar_usuario(struct Usuario *usu);
void cumplir_anios_por_ptr(struct Usuario *usu);
void cumplir_anios_por_valor(struct Usuario usu);
void cumplir_anios_cantidad(struct Usuario* usu, int cantidad_anios);

int main()
{
	struct Usuario mi_usuario;
	printf("Ejemplos de estructuras:\n");
	strcpy_s(mi_usuario.nombre, 50, "German Caballero");
	strcpy_s(mi_usuario.email, 321, "german@mentira.es");
	strcpy_s(mi_usuario.direccion, 101, "Calle Me la Invento, 30");
	mi_usuario.edad = 27;

	printf("Mostramos el nombre: %s, y ahora mostraremos el resto en una funcion\n", mi_usuario.nombre);
	// Aquí pasamos con &, la dirección de memoria de mi_usuario
	mostrar_usuario( & mi_usuario);
	cumplir_anios_por_ptr( & mi_usuario);	// Aquí modificamos esta variable
	mostrar_usuario( & mi_usuario);
	cumplir_anios_por_valor( mi_usuario );	// Por valor no se modifica esta variable
	mostrar_usuario( & mi_usuario);

	cumplir_anios_cantidad(&mi_usuario, -5);
	mostrar_usuario(&mi_usuario);

	struct Usuario otroUsuario;
	// .... rellenar los datos
	strcpy_s(otroUsuario.nombre, 50, "Javier D.");
	strcpy_s(otroUsuario.email, 321, "asdsdaf@fdgfgfg");
	strcpy_s(otroUsuario.direccion, 101, "Calle Tu calle");
	mostrar_usuario(&otroUsuario);

	struct Usuario pedro;
	strcpy_s(pedro.nombre, 50, "Pedro amigo de Heidi");
	strcpy_s(pedro.email, 321, "Pedro@Heidi.com");
	strcpy_s(pedro.direccion, 101, "La pradera");
	pedro.edad = 10;
	mostrar_usuario(&pedro);

	struct Usuario* usuarios[20];
	usuarios[0] = & mi_usuario;
	usuarios[1] = & otroUsuario;
	usuarios[2] = & pedro;
	printf("El tamaño del array es de %i\n", sizeof(usuarios));
	cumplir_anios_cantidad( usuarios[2], 5);
	mostrar_usuario(usuarios[2]);
	mostrar_usuario(&pedro);
	usuarios[3] = &mi_usuario;	// Referenciando por duplicado
	// Ejemplo de instanciación de una estructura:
	// Instanciación es la creación de una instancia, es decir, reservar el espacio de memoria necesario para albergar una estructura, array, objeto....
	usuarios[4] = malloc(sizeof(struct Usuario));
	printf("El entero que devuelve malloc: %p\n", usuarios[4]);
	strcpy_s(usuarios[4]->nombre, 50, "Yesica M.");
	strcpy_s(usuarios[4]->email, 321, "yes@email.com");
	strcpy_s(usuarios[4]->direccion, 101, "En algun lado");
	usuarios[4]->edad = 30;
	mostrar_usuario(usuarios[4]);
	printf("AL PRINCIPIO: %i\n", *((int*)(((char*)usuarios[4]) + sizeof(usuarios[4]->nombre) + 321 + 101)));
	// Es lo mismo que printf("AL PRINCIPIO: %i\n", usuarios[4]->edad);
	free(usuarios[4]);
	FILE* fich;
	fopen_s(&fich, "fich_pa_javier.txt", "w");
	fprintf(fich, "%s,%s,%s,%i\n",
		otroUsuario.nombre,
		otroUsuario.direccion,
		otroUsuario.email,
		otroUsuario.edad
		);
	
	fclose(fich);
}
// Aquí recibimos un puntero a estructura
void mostrar_usuario(struct Usuario* usu) {
	// Para leer sus propiedad en vez del '.' usamos la flecha:  "->" y leemos así: nombre del usu...
	printf("Datos usuario %s:\n - Email: %s - Edad: %i\nDireccion: %s\n\n", usu->nombre, usu->email, usu->edad, usu->direccion);
}
// Al pasar por puntero modificamos la variable externa (el argumento)
void cumplir_anios_por_ptr(struct Usuario* usuario) {
	usuario->edad++;
}
// Al pasar por valor, que NO es una direcc mem, NO no modificamos el argumento, porque se crea una copia de la estructura.
void cumplir_anios_por_valor(struct Usuario user) {
	user.edad++;
	mostrar_usuario(&user);
}
// Ej 1: Añadir campo, propiedad, variable miembro dirección (máximo 100 caracteres).
// Ej 2: Crear una función que cumpla años pero con un parámetro extra: cantidad_anios

// Al pasar por puntero modificamos la variable externa (el argumento)
void cumplir_anios_cantidad(struct Usuario* u, int cantidad_anios) {
	u->edad += cantidad_anios;
}