#include <stdio.h>
#include <string.h>
#include <stdbool.h>

// Crear otro proyecto, con una estructura Animal: nombre, número patas, booleano volador (sí/no), peso medio (decimales).(*) 
// incluir stdbool.h. true/false(*) 
// Crear un animal y rellenar sus datos.(*)  
// Crear una función para mostrar sus valores(*) 
// Otra función para modificar el peso por parámetro
// Una función para modficar el peso pidíendoselo al usuario.

struct Animal {
	char nombre[50];
	short int num_patas;	// 2 bytes
	bool volador;
	float peso_medio;
	long long int cantidad_mundo;	// 8 bytes 
};
void mostrar_animal(struct Animal* animal);
void modificar_peso_animal(struct Animal* animal, float nuevo_peso);
void pedir_peso_animal(struct Animal* animal);

int main()
{
	struct Animal perro;
	strcpy_s(perro.nombre, 50, "Perro");
	perro.num_patas = 4;     perro.peso_medio = 15.5;   perro.volador = false;	
	modificar_peso_animal(&perro, 12.3);
	mostrar_animal(&perro);

	struct Animal loro;
	strcpy_s(loro.nombre, 50, "Loro Cachondo");
	loro.num_patas = 2;      loro.peso_medio = 1.5;     loro.volador = true;
	pedir_peso_animal(&loro);
	mostrar_animal(&loro);

	// Aquí, un array con esos 2 animales y uno nuevo 3 instanciado con malloc
	struct Animal* animales[3];
	animales[0] = &perro;
	animales[1] = &loro;
	int bytes = sizeof(struct Animal);
	animales[2] = malloc(bytes);
	printf("Reservados %i bytes\nDi el animal:\n", bytes);
	scanf_s("%s", animales[2]->nombre, 50);
	pedir_peso_animal(animales[2]);
	printf("Introduzca las patas:\n");
	scanf_s("%i", &(animales[2]->num_patas));
	animales[2]->volador = false;
	mostrar_animal(animales[2]);
	float media = 0;
	media = calcular_media(animales, 2);
	printf("Peso medio todos animales: %f\n", media);
	free(animales[2]);
}
/* Aquí una función que reciba un puntero a Animal (pero que en realidad es un array). Necesitaréis el tamaño que calcule la media de los animales (la suma total entre la cantidad/tamaño, y que lo devuelva (return) */
// Los dos asteriscos: Es que el array de punteros lo recibimos con un puntero a puntero.
float calcular_media(struct Animal** animales, int tam) {
	float suma_pesos = 0;
	for (int i = 0; i < tam; i++) {
		suma_pesos += (*(animales + i))->peso_medio; 
			// Lo mismo que sumar:  animales[i]->peso_medio
			// Con aritmética de punteros: que el calculador 
			// calcula cuanto ocupada cada Animal, y suma esos bytes al puntero original
	}
	float media = suma_pesos / ((float) tam);
	printf("Te compilas?\n");
	return media ;
}
// Esta función, aunque es independiente en el código, en fondo está vincalada a la estructura Animal
void mostrar_animal(struct Animal* animal) {
	printf("Animal %s:\n - Patas: %i, Peso medio: %.2f, %s\n", 
		animal->nombre, 
		animal->num_patas, 
		animal->peso_medio, 
		animal->volador ? "volador" : "no volador" );
}
void modificar_peso_animal(struct Animal* animal, float nuevo_peso) {
	animal->peso_medio = nuevo_peso;
}
void pedir_peso_animal(struct Animal* animal) {

	printf("Introduzca el peso de %s:\n", animal->nombre);
	scanf_s("%f", & animal->peso_medio);
}



