#include "cabecera.h"

// N�mero de 32 bits, 8 bytes
// 1111 0011 1000 1101 0110 1001 0101 0010	= F38D6952
// Cada grupo de 4 se recoje en una cifra hexadecimal:
// Un n�mero entre 0 y 16. 0-9  A-F	(A=10, B=11, C=12, F=15)
// F     3     8    D     6    9   5    2

void ejemplo_ptr_entero() {
	char ch1 = 30;
	int i1 = 55;
	long long l1 = 3000000000;

	printf("ch1      -   i1     - l1\n");
	printf("%i       -   %i     - %lli\n", ch1, i1, l1);
	printf("%p - %p - %p\n", &ch1, &i1, &l1);
	// Con & cogemos la direcci�n de memoria en un entero
	int dir_ch1 = (int)&ch1;
	// Con * podemos crear un puntero a un tipo de dato:
	char* ptr_ch1 = dir_ch1;
	// Un puntero es un tipo de variable especial que guarda una direcci�n de memoria, es decir, un n�mero entero

	// Con * tambi�n podemos cambiar el contenido de una direcc memoria
	// Tambi�n es el operador de indirecci�n (operador de contenido)
	*(ptr_ch1) = 33;	// ch1 = 33

	// EL n� i1, es entero, ocupa 4 bytes.
	// Como guarda un 55, es decir, 00 00 00 37
	// Cogemos un puntero a entero:
	int dir_i1 = &i1;
	char* ptr_i1 = dir_i1 + 1;
	// Cambiamos a 00 00 01 37, es decir, a 311
	// En lugar de directamente poner i1 = 311, vamos a cambiar ese byte
	*(ptr_i1) = 1;

	printf("%i       -   %i     - %lli\n", ch1, i1, l1);
}