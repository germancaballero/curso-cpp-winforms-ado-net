#include "cabecera.h"

void ejemplo_array_int() {
	// Un array en C/C++, es un conjunto de elementos del MISMO TIPO y de longitud FIJA
	// Los arrays se declaran y se usan con corchetes [ ]
	int notas[5];	// Aqu� estamos reservando la memoria para 5 n�meros enteros: Es decir, 4 bytes x 5 = 20 bytes. Aqu� en la declaraci�n el 5 es el tama�o

	// Para rellenarlo, se pone eentre corchetes el �ndice/la posici�n del elemento en el array. El primer elemento empieza en el CERO
	notas[0] = 4;	// Como si crearamos 5 variables enteras:  int notas_0;
	notas[1] = 9;											// int notas_1;
	notas[2] = 5;											// int notas_2;
	notas[3] = 10;
	notas[4] = 3;	// Por lo tanto, el �ltimo, es la longitud del array menos 1.
	printf("Mostrando las 5 notas: \n");
	int pos = 0;
	while (pos < 5) {
		printf("Nota %i = %i \n", pos, notas[pos]);
		pos++;
	}
}
// Ejercicio: Crear un array de 6 elementos con los primeros 6 n�meros cuadrados:
// 1, 4, 9, 16, 25 y el 36: Hacedlo con 2 bucles, uno para rellenar y otro para mostrar los resultados
void ejercicio_cuadrados() {
	int cuadrados[6];
	int i = 0;
	while (i < 6) {
		cuadrados[i] = (i + 1) * (i + 1);
		i++;
	}
	printf("Mostrando cuadrados:\n");
	i = 0;
	while (i < 6) {
		printf("Cuadrado de %i = %i \n", i + 1, cuadrados[i]);
		i++;
	}
}
// Ejercicio: Dados dos arrays de 4 elementos, con valores aleatorios:  A1=4,2,6,3  A2=1,5,2,1  
// (1) calcular la suma de los todos los elementos de los dos arrays 
//(2) crear un array con la suma de las parejas de los elementos
// A1=4,2,6,3  A2=1,5,2,1   suma=24   array_suma=5, 7, 8, 4
void ejercicio_suma_arrays_while() {
	int a1[4] = { 4, 2, 6, 3 };
	int a2[4] = { 1, 5, 2, 1 };
	int i = 0, suma = 0;
	while (i < 4) {
		suma = suma + a1[i] + a2[i];
		i++;
	}
	printf("Suma total = %i\n", suma);
}
void ejercicio_suma_arrays_for() {
	int a1[4] = { 4, 2, 6, 3 };
	int a2[4] = { 1, 5, 2, 1 };
	int array_suma[4];

	int i, suma = 0;
	// (1)
	for (i = 0; i < 4; i++) {	// El bucle for es un while resumido
		suma += a1[i] + a2[i];		
	}
	printf("Suma total = %i\n", suma);
	//   (2)
	for (i = 0; i < 4; i++) {	
		array_suma[i] = a1[i] + a2[i];
		printf("Suma [%i] = %i\n", i, array_suma[i]);
	}
	/* Desarrollar un funci�n que permita ingresar un vector de 8 elementos, e informe :
	    El valor acumulado de todos los elementos del vector.
		El valor acumulado de los elementos del vector que sean mayores a 36.
		Cantidad de valores mayores a 50. */
	// scanf_s("%i", & nombre_array[2]);
}
void ejercicio_arrays_8_elementos() {
	int a1[8]; // = { 4, 2, 6, 3, 50, 60, 22, 11 };

	int i, suma = 0, sumaMayores36 = 0, cantMayores50 = 0;

	for (i = 0; i < 8; i++) 
	{
		printf("\nIntroduzca la pos %i: \n", i);
		scanf_s("%i", & a1[i]);
	}
	// (1)
	for (i = 0; i < 8; i++) {	// El bucle for es un while resumido
		suma += a1[i];
		if (a1[i] > 36) {
			sumaMayores36 += a1[i];
		}
		if (a1[i] > 50) {
			cantMayores50 ++;
		}
	}
	printf("Suma: %i, total > 36:  %i, cantidad > 50: %i", suma, sumaMayores36, cantMayores50);
}
void ejercicio_comprobar_menores_for() {
	int vector[10] = 
		//	{ 1, 2, 3, 5, 4, 6, 3, 5, 6, 3 };
	     { 1, 4, 7, 7, 9, 10, 11, 14, 14, 20 };
	// Variables auxiliares
	int ordenado = 1;	// Por defecto, suponemos que est� ordenado
	for (int i = 0; i < 9; i++) {
		// Condici�n
		if (vector[i] > vector[i + 1]) {
			ordenado = 0;// 0 para indicar que NO est� ordenado
			break;
		}
	}
	// Otra condici�n
	if (ordenado == 1)
		printf("Ordenado\n");
	else
		printf("No ordenado\n");
}

void ejercicio_comprobar_menores_while() {
	int vector[10] =
		{ 1, 2, 6, 5, 4, 6, 3, 5, 6, 3 };
	//{ 1, 4, 7, 7, 9, 10, 11, 14, 14, 20 };
	// Variables auxiliares
	int ordenado = 1;	// Por defecto, suponemos que est� ordenado
	int i = 0;
	while (i < 9 && ordenado == 1) {
		// Condici�n
		if (vector[i] > vector[i + 1]) {
			ordenado = 0;// 0 para indicar que NO est� ordenado
		}
		i++;
	}
	// Otra condici�n
	if (ordenado == 1)
		printf("Ordenado\n");
	else
		printf("No ordenado\n");
}