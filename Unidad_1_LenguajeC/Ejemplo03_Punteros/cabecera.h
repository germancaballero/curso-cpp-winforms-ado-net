#pragma once

#include <stdio.h>

void ejemplo_ptr_entero();
void ejemplo_array_int();
void ejercicio_cuadrados();
void ejercicio_suma_arrays_while();
void ejercicio_suma_arrays_for();
void ejercicio_arrays_8_elementos();
void ejercicio_comprobar_menores_for();
void ejercicio_comprobar_menores_while();
