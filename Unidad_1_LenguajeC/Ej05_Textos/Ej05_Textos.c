#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

/* Funciones de cadenas de caracteres típicas */
void ejemplos_funciones_textos();
void ejemplos_reserva_memoria();
void ejercicio_direccion_mayus_2();
void ejemplo_array_int_como_puntero();
void mostrar_array(int ln[], int tam);
void array_x2(int ln[], int tam);

// Los textos son arrays de caracteres, donde cada caracter es un tipo "char" (osea, un número entero entre -128 y 127)
// El tipo char, además de usarse como número, es un carácter simple, UNO sólo, y tiene que ir entre comillas simples.
int main()
{
	printf("Textos. Esto es un texto, entre comillas dobles, pero esto si que es un array constante. Para manipularlo necesitamos arrays de char.\n");
	// La 'A' mayuscula tiene codigo ASCII 65. 'a' minus -> 97
	char miNombre[10] = { 'G', 'e', 'r', 'm', 97, 'n', 7, '\0' };

	printf("Mi nombre es %s.\n", miNombre);
	//ejemplos_funciones_textos();
	//ejemplos_reserva_memoria();
	// ejercicio_direccion_mayus_2();
	ejemplo_array_int_como_puntero();
}
void ejemplos_funciones_textos() {
	char texto[30];
	int longitud = strlen("En un lugar de la Mancha...\0");
	printf("Longitud:  %i\n", longitud);
	printf("Tamano char:  %i\n", sizeof(char));
	strcpy_s(texto, sizeof(texto),"En un lugar de la Mancha...\0");
	printf("El texto es:\n %s \n", texto);
}
void ejemplos_reserva_memoria() {
	int longitud = strlen("Para un texto de longitud variable, necesitamos reserva memoria");
	char* texto;
	texto = malloc((longitud + 1) * sizeof(char));
	strcpy_s(texto, longitud + 1, "Para un texto de longitud variable, necesitamos reserva memoria");
	printf("El texto es %s\n", texto);
	// El asterisco también se usa como operador de indirección (contenido de un puntero)
	printf("La primera letra  '%c'\n", *(texto));
	printf("La cuarte letra  '%c'\n", *(texto + 3));
	// ¡¡Que no se nos olvide liberar memoria
	free(texto);
}
/* Ejercicio:
Pedir al usuario cual es su dirección (la guardamos primero en un array de 100 elementos, pero luego en un puntero reservando la memoria exacta de la longitud de la dirección), y convertirla y mostrarla en mayúsculas con espacios entre letras :
Si el usuario escribe: Calle Laboral, 10 Madrid. Tiene que salir:
C A L L E    L A B O R A L, 1 0    M A D R I D */
/*0	1	2	3	4	5	6	7	8	9	10	11	12	….	100
C	A	L	L	E	\0	&	c	3	^	'	_	R		
C	 	A	 	L	 	L	 	E	 	\0	_	R		
*/
void ejercicio_direccion_mayus_2() {
	char direccion[100];
	printf("Introduzca su direccion:\n");
	scanf_s("%99s", direccion, (unsigned) _countof(direccion));
	printf("Tu direccion es:\n %s. \n", direccion);
	int longitud = (int) strlen(direccion);
	// Aquí declarmos no un char, es una dirección de memoria a un char, lo que se denomina en C (C++) PUNTERO.
	int longitud_final = longitud * 2 + 1;
	char* palabra_direcc = malloc(longitud_final * sizeof(char));
	strcpy_s(palabra_direcc, longitud_final, direccion);
	printf("En puntero:\n %s. \n", palabra_direcc);
	_strupr_s(palabra_direcc, longitud_final);
	printf("En mayusculas:\n %s. \n", palabra_direcc);
		
	/*for (int i = 0; i < 100; i++) {	// PARA MOSTRAR EL INTERIOR:
		printf("[%i]='%c', ", *(palabra_direcc + i), *(palabra_direcc + i));
	}*/
	for (int i = longitud; i > 0; i--) {
		*(palabra_direcc + i * 2) = *(palabra_direcc + i);
		*(palabra_direcc + i * 2 - 1) = 32; /// ' ';
	}
	printf("Y con espacios:\n %s. \n", palabra_direcc);
	free(palabra_direcc);
}

void ejercicio_direccion_puntero_como_array() {
	char direccion[100];
	printf("Introduzca su direccion:\n");
	scanf_s("%99s", direccion, (unsigned)_countof(direccion));
	printf("Tu direccion es:\n %s. \n", direccion);
	int longitud = (int)strlen(direccion);
	// Aquí declarmos no un char, es una dirección de memoria a un char, lo que se denomina en C (C++) PUNTERO.
	int longitud_final = longitud * 2 + 1;
	char* ptr_palabra_direcc = malloc(longitud_final * sizeof(char));
	strcpy_s(ptr_palabra_direcc, longitud_final, direccion);
	printf("En puntero:\n %s. \n", ptr_palabra_direcc);
	_strupr_s(ptr_palabra_direcc, longitud_final);
	printf("En mayusculas:\n %s. \n", ptr_palabra_direcc);

	for (int i = longitud; i > 0; i--) {
		ptr_palabra_direcc[i * 2] = ptr_palabra_direcc[i];
		ptr_palabra_direcc[i * 2 - 1] = ' ';	// 32
	}
	printf("Y con espacios:\n %s. \n", ptr_palabra_direcc);
	free(ptr_palabra_direcc);
}
void ejemplo_array_int_como_puntero() {
	int lista_numeros[5] = { 10, 20, 30, 40, 100 };
	int taman = 5;
	// int *otroPuntero = lista_numeros + 2;
	//otroPuntero      = &lista_numeros[2];
	//lista_numeros = otroPuntero +1;
	// El array SÍ se modifica dentro la función. La variable simple taman NO.
	array_x2(lista_numeros, taman);
	mostrar_array(lista_numeros, taman);
}
// ln se pasa por referencia. Porque los arrays son direcciones de memoria (punteros)
// tam se pasa por valor: El parámtero 'tam' es una copia diferente al argumento 'taman'
void array_x2(int *lista_n, int tam) {
	for (int i = 0; i < tam; i++) {
		*(lista_n + i) = *(lista_n + i) * 2;	// Esta modificación sí tiene fuera
	}
	tam = 200;	// Esto no tiene efecto fuera
}
void mostrar_array(int ln[], int tam) {
	for (int i = 0; i < tam; i++) {
		printf("\n - %i -> %i", i, ln[i]);
	}
}