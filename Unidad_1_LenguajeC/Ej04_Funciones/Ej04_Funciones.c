// Ej04_Funciones.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <stdio.h>
#include <stdbool.h>

// Declaración: 
// <tipo_devuelto>  <nombre_funcion> (  <param_1> , <param_2>...);
// Implementación
// <tipo_devuelto>  <nombre_funcion> (  <param_1> , <param_2>...) 
//  {
// Código que sea, otras variables, bucles, condciones, llamadas a otras funciones...
//    Si queremos terminar la función usamos
//    return   <valor_devuelto>;
//  }
float suma3(float);
float funcionXY(float, float);
int mayor3(int x1, int x2, int x3);
int mayor4(int x1, int x2, int x3, int x4);
int sumaArray(int numeros[], int tamano);
int maximoEnArray(int numeros[], int tamano);
int maximoEnArrayRapido(int numeros[], int tamano);
bool siEstaOrdenado(int numeros[], int tamano);


int main()
{
	printf(" 5 + 3 = %.2f \n", suma3(5));
	// O podemos usar una variable intermedia
	float resultado = suma3(13);
	printf("resultado (13 + 3) = %.2f \n", resultado);
	// La llamada a la función:
	// Consiste en poner el nombre de la función y entre paréntesis los argumentos que entregamos a la función. Los argumentos se copiarán en los parámetros. Lo que devuelve la función lo podemos usar como queramos, guardándolo en una variable o directamente
	float resXY = funcionXY(3, 7);
	printf("resultado (3 + 2 * 7) = %.2f \n", resXY);

	printf("El total de las operaciones es %f", ( resultado + resXY));

	printf("El mayor de 1, 3, 5 es %i \n", mayor3(1, 3, 5));
	printf("El mayor de 4, 3, 1 es %i \n", mayor3(4, 3, 1));
	printf("El mayor de 1, 8, 5 es %i \n", mayor3(1, 8, 5));

	int arr_1[4] = { 1, 5, 2, 7 };
	int arr_2[4] = { 9, 5, 1, 4 };
	int arr_3[4] = { 2, 1, 6, 5 };

	int mayores[4];
	for (int i = 0; i < 4; i++) {
		mayores[i] = mayor3(arr_1[i], arr_2[i], arr_3[i]);
	}
	printf("Mayores: %i, %i, %i, %i\n", mayores[0], mayores[1], mayores[2], mayores[3]);
	printf("Suma array 1: %i\n", sumaArray(arr_1, 4));

	printf("\nMaximo array 1: %i\n", maximoEnArrayRapido(arr_1, 4));
	printf("\nMaximo array 2: %i\n", maximoEnArrayRapido(arr_2, 4));
	printf("\nMaximo array 2: %i\n", maximoEnArrayRapido(arr_3, 4));

	int arr_0[1] = { 1 };
	int arr_4[4] = { 2, 3, 4, 6 };
	int arr_5[10] = { 1, 3, 1, 7, 8, 7, 8, 8, 2, 8 };
	int arr_6[6] = { 2, 9, 3, 7, 5, 2 };
	printf("\nMaximo array 0: %i\n", maximoEnArrayRapido(arr_0, 1));
	printf("\nMaximo array 4: %i\n", maximoEnArrayRapido(arr_4, 4));
	printf("\nMaximo array 5: %i\n", maximoEnArrayRapido(arr_5, 10));
	printf("\nMaximo array 6: %i\n", maximoEnArrayRapido(arr_6, 6));

	if (siEstaOrdenado(arr_6, 6)) {
		printf("Array 6 esta ordenado\n");
	}
	else {
		printf("Array 6 esta NO ordenado\n");
	}
	if (siEstaOrdenado(arr_4, 4)) {
		printf("Array 4 esta ordenado\n");
	}
	else {
		printf("Array 4 esta NO ordenado\n");
	}
	
}
// En la implementación, la función recibe PARÁMETROS
//  que son variables locales que reciben los valores 
float suma3(float x) {
	return x + 3;
}
float funcionXY(float x, float y) {
	return x + 2 * y;
}
int mayor3(int x1, int x2, int x3) {
	if (x1 > x2 && x1 > x3) {
		return x1;
	}
	else {
		if (x2 > x3)
			return x2;
		else
			return x3;
	}
}
int mayor4(int x1, int x2, int x3, int x4) {
	if (x1 > x2 && x1 > x3 && x1 > x4) {
		return x1;
	}
	else  if (x2 > x3 && x2 > x4)
		return x2;
	else  if (x3 > x4)
		return x3;
	else
		return x4;		
}
// 0 + 1 + 1+ 0 + 1
int mayor5(int x0, int x1, int x2, int x3, int x4) {
	if ((x0 > x1 && x0 > x2) && x0 > x3 && x0 > x4) {
		return x0;
	}
	else  if (x1 > x2 && x1 > x3 && x1 > x4)
		return x1;
	else  if (x2 > x3 && x2 > x4)
		return x2;
	else if (x3 > x4)
		return x3;
	else
		return x4;
}
int sumaArray(int numeros[], int tamano) {
	int total = 0;
	for (int i = 0; i < tamano; i++) {
		total += numeros[i];
	}
	return total;
}
int maximoEnArray(int numeros[], int tamano) { // { 9, 5, 1, 4 };
	int maximo = numeros[0];
	bool esMayor = false;
	// Llegamos a tamaño - 1 tanto con < tamaño
	// como con <= tamano - 1
	int pos_i = 0;
	while ( pos_i < tamano - 1 && esMayor == false) {
		esMayor = numeros[pos_i] > numeros[pos_i + 1];
		int pos_j = pos_i + 2;	// + 2 porque ya hemos preguntado por los 2 primeros
		while ( pos_j < tamano && esMayor == true) {
			esMayor = esMayor && numeros[pos_i] > numeros[pos_j];	
			pos_j++;
		}
		if (esMayor == true) {
			maximo = numeros[pos_i];
			return maximo;
		}
		pos_i++;
	}
	if (esMayor)
		return maximo;
	else if (tamano == 1)
		return numeros[0];
	else
		return numeros[tamano - 1] ; 
		// No podemos devolver 0, hay que devolver el último elemento
}
// EJercicio 1: función que calcule el valor máximo de sus elementos.
int maximoEnArrayRapido(int numeros[], int tamano) { // { 9, 5, 1, 20, 4 };
	int maximo = numeros[0];
	int pos_i = 1;
	while (pos_i < tamano - 1) {
		if (numeros[pos_i] > maximo) {
			maximo = numeros[pos_i];
		}
		pos_i++;
	}
	return maximo;
	// No podemos devolver 0, hay que devolver el último elemento
}

// EJercicio 2: Función que devuelva true (1) o false (0) si está ordenada
bool siEstaOrdenado(int numeros[], int tamano) { 
	// Variables auxiliares
	bool ordenado = true;	// Por defecto, suponemos que está ordenado
	for (int i = 0; i < tamano - 1; i++) {
		// Condición
		if (numeros[i] > numeros[i + 1]) {
			ordenado = false;// 0 para indicar que NO está ordenado
			break;
		}
	}
	return ordenado;
	// No podemos devolver 0, hay que devolver el último elemento
}
void ejercicio_comprobar_menores_for() {
	int vector[10] =
		//	{ 1, 2, 3, 5, 4, 6, 3, 5, 6, 3 };
	{ 1, 4, 7, 7, 9, 10, 11, 14, 14, 20 };
	// Variables auxiliares
	int ordenado = 1;	// Por defecto, suponemos que está ordenado
	for (int i = 0; i < 9; i++) {
		// Condición
		if (vector[i] > vector[i + 1]) {
			ordenado = 0;// 0 para indicar que NO está ordenado
			break;
		}
	}
	// Otra condición
	if (ordenado == 1)
		printf("Ordenado\n");
	else
		printf("No ordenado\n");
}
// EJercicio 3: Función que ordene un array
