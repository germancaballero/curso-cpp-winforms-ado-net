// Ej02_IntroCine.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <stdio.h>
#include "operadores.h"
#include "condicional_simple.h"
#include "condicionales_compuesto.h"

int main()
{
    printf("Programa introduccion algoritmo cine\n");

    calculo_oro();
    //ejemplo_operadores();
    // probando_int_float();
    probando_long_double();
    comprobar_edad();
    // Ejercicio: Función que compruebe la distancia máxima al cine y la distancia real del cine
    comprobar_distancia();
    comprobar_valoracion_cine();
    comprobar_valoracion_director();
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
