#include "condicional_simple.h"

void comprobar_edad() {

	// Entero de 8 bit (1 byte): Rango -128 a +127
	char edad_pegui = 18;
	// Entero positivo de 8 bit: Rango 0 a 255
	unsigned char edad_usuario = 5;
	printf("Edad pegui: %d\nEdad usuario:  %d \n", edad_pegui, edad_usuario);
	printf("\n");

	int diferencia_edad = edad_usuario - edad_pegui;
	printf("Diferencia edad: %d\n", diferencia_edad);

	// Condicionales nos permiten bifurcar la ejecuci�n del programa
	// Condicional simple: if (condici�n)  instrucci�n;
	if (edad_usuario > edad_pegui)  printf("Puedes ver la peli.\n");
	// En dos o m�s lineas:
	if (edad_usuario < edad_pegui)
		printf("NO puedes ver la peli\n");
}

void comprobar_distancia() {
	unsigned char distancia_max = 15;
	// Variable tipo int:   4 comprobar_distanciabytes
	int distancia_cine = 10;
	if (distancia_cine < distancia_max)
		printf("El cine esta cerca\n");
	if (distancia_cine > distancia_max)
		printf("El cine esta lejos\n");
}
