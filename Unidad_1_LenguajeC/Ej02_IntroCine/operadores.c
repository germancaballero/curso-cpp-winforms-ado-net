#include "operadores.h"

void ejemplo_operadores() {
	printf("Multiplicacion:     3 * 4 =  %d\n", 3 * 4);
	printf("Division:           17 / 3 = %d\n", 17 / 3);
	printf("Modulo (resto div): 17 %% 3 = %d\n", 17 % 3);
}
void probando_int_float() {
	// Rango de -2.000.000.000 a +2.000.000.000
	int op1 = 1234567890;
	int op2 = 3;
	// Conversi�n de datos: se pone el tipo entre par�ntesis antes del valor
	float div = (float) op1 / (float) op2;
	printf("Division: %d / %d = %f\n", op1, op2, div);
}
void probando_long_double() {
	int64_t  pib = 1244757000000;
	printf("Tamanho int64_t = %d\n", sizeof(int64_t));
	printf("PIB anual = %lld\n", pib);
	double pi = 3.141592653589793;
	printf("Tamanho double = %d\n", sizeof(double));
	printf("N� PI = %e\n", pi);
}
void calculo_oro()
{
	float euro_gramo = 54.3f;
	float tasa_crecimiento = 4.3f;	// Porcentaje
	{
		float precio_2021 = euro_gramo * (100.0f + tasa_crecimiento) / 100.0f;
		printf("Tasa oro actual = %f, precio 2021 = %f\n",
			tasa_crecimiento,
			precio_2021);
		// 126,03350033817478122828518672173
		// Los bucles nos ayudan a repeteir tantas veces como queramos una instrucci�n o un bloque
		// while se repite MIENTRAS la condici�n sea cierta
		int ano = 2020;
		float precio_por_ano = euro_gramo;
		int ano_final = 0;
		printf("Introduzca anho calculo:\n");
		scanf_s("%i", & ano_final);
		while (ano < ano_final ) {
			ano = ano + 1;
			precio_por_ano = precio_por_ano * (100 + tasa_crecimiento) / 100;
		}		
		printf("Anho actual = %d, pricio oro = %f\n", ano, precio_por_ano);
	}
}
