#include "condicionales_compuesto.h"

void comprobar_valoracion_cine() {
	float valoracion_minima;
	float valoracion_peli;

	valoracion_minima = 5;
	valoracion_peli = 6.5;
	printf("La peli tiene %f puntos de valoracion\n", valoracion_peli);
	if (valoracion_peli > valoracion_minima)
		printf("La peli tiene valoracion suficiente\n");
	else 
		printf("La peli en mala\n");

	// Condicionales de varias opciones
	if (valoracion_peli < 0)
		printf("La valoracion no puede ser negativa\n");
	else if (valoracion_peli > 10)
		printf("La valoracion es excesiva\n");
	else
		printf("Valoracion correcta\n");
}
/* EJERCICIO:  2 funciones diferentes
Valoraci�n del director		0 - 10 Decimal
Cr�ticas : N� de buenas, n� de regulares, n� de malas(3 n�meros enteros)
*/

void comprobar_valoracion_director() {
	float valoracion_minima;
	float valoracion_director;

	valoracion_minima = 5;
	printf("Ingrese la valoracion del director\n");
	scanf_s("%f", &valoracion_director);
	printf("El director tiene %f puntos de valoracion\n", valoracion_director);
	if (valoracion_director > valoracion_minima)
		printf("Eldirector tiene valoracion suficiente\n");
	else
		printf("Eldirector en malo\n");

	// Condicionales de varias opciones
	if (valoracion_director < 0) {
		printf("La valoracion no puede ser negativa\n");
		printf("Otra linea\n");
	}
	else if (valoracion_director > 10)
		printf("La valoracion es excesiva\n");
	else
		printf("Valoracion correcta\n");
}