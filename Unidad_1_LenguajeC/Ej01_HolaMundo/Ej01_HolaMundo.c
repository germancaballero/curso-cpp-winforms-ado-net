// Ej01_HolaMundo.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//
#include "funcion_holamundo.h"
#include <stdio.h>  // Standar Input Output

// Las declaraciones, al principio:
void hola_mundo_declarada(void);

// Implementando una función (también es una declaración)
void hola_mundo(void) {
    printf("Funcion hola mundo! \n");
}
int main()
{
    hola_mundo();
    // Formato C++
    printf("Hola mundo!\n");
    // Formato C
    // Invocamos (llamamos ) a nuesstra función
    hola_mundo();
    hola_mundo_declarada();
    hola_mundo_fun_ext();

    printf("\nTipos de datos");

    int numero = 100;
    printf("\nNumero es %d . ", numero);
}

void hola_mundo_declarada() {
    printf("Funcion hola mundo declarada! \n");
}

// Ejecutar programa: Ctrl + F5 o menú Depurar > Iniciar sin depurar
// Depurar programa: F5 o menú Depurar > Iniciar depuración

// Sugerencias para primeros pasos: 1. Use la ventana del Explorador de soluciones para agregar y administrar archivos
//   2. Use la ventana de Team Explorer para conectar con el control de código fuente
//   3. Use la ventana de salida para ver la salida de compilación y otros mensajes
//   4. Use la ventana Lista de errores para ver los errores
//   5. Vaya a Proyecto > Agregar nuevo elemento para crear nuevos archivos de código, o a Proyecto > Agregar elemento existente para agregar archivos de código existentes al proyecto
//   6. En el futuro, para volver a abrir este proyecto, vaya a Archivo > Abrir > Proyecto y seleccione el archivo .sln
