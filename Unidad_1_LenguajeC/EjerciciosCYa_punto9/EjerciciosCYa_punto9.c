#include <stdio.h>

void ejercicio_notas_alumnos();
void ejercicio_25_num_serie();
void ejercicio_pares_impares();
void ejercicio_mayor_3();

int main()
{
    // ejercicio_notas_alumnos();
    // ejercicio_25_num_serie();
    // ejercicio_pares_impares();
    ejercicio_mayor_3();
}

void ejercicio_notas_alumnos() {
    int contador = 10;
    int notas_mayores_o_i = 0;
    int notas_menores = 0;
    while (contador > 0) {
        printf("Ingrese nota: \n");
        float nota_alumno = 0;
        scanf_s("%f", &nota_alumno);

        if (nota_alumno >= 7) {
            notas_mayores_o_i = notas_mayores_o_i + 1;
        }
        else {
            notas_menores = notas_menores + 1;
        }
        printf("Nota alumno: %f\n", nota_alumno);
        // contador = contador + 1;  contador += 1;  contador++;
        contador--; 
        // Es lo mismo contador -=1 ó contador = contador - 1
    }
    printf("Notas mayores o iguales: %d  y menores: %d\n", notas_mayores_o_i, notas_menores);
}
// Realizar un programa que imprima 25 términos de la serie 11 - 22 - 33 - 44, etc. (No se ingresan valores por teclado) 
void ejercicio_25_num_serie() {
    int num = 0;
    while (num < 25 * 11)
        printf("- Num serie %d: %d\n", num/11, num += 11);

}
/*Mostrar todos los múltiplos de 8 que hay hasta el valor 500.
Debe aparecer en pantalla 8 - 16 - 24, etc.*/
void ejercicio_pares_impares() {
    int n = 0, c = 0, 
        num_usu, pares = 0, impares = 0;
    printf("Introduzca veces:\n");
    scanf_s("%d", &n);
    while (c < n ) {
        printf("Escribe el num %d:\n", c);
        scanf_s("%d", &num_usu);
        if (num_usu % 2 == 0)
            pares++;
        else
            impares++;
        c++;
    }
    printf("Pares %d, impares %d:\n", pares, impares);
}
void ejercicio_mayor_3() {
    int num1, num2, num3, num4;
    printf("Ingrese primer valor:");
    scanf_s("%i", &num1);
    printf("Ingrese segundo valor:");
    scanf_s("%i", &num2);
    printf("Ingrese tercer valor:");
    scanf_s("%i", &num3);
    printf("Ingrese cuarto valor:");
    scanf_s("%i", &num4);
    if (num1 > num2 && num1 > num3 && num1 > num4)
    {
        printf("%i", num1);
    }
    else
    {
        // Sig. que cualquiera menos el num1 puede ser el mayor

        if (num2 > num3 && num2 > num4)
        {
            printf("%i", num2);
        }
        else
        {
            if (num3 > num4)
                printf("%i", num3);
            else
                printf("%i", num4);
        }
    }
}